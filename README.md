# fzf for Pearl

🌸 A command-line fuzzy finder

## Details

- fzf: https://github.com/junegunn/fzf
- Pearl: https://github.com/pearl-core/pearl
