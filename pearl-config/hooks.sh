function post_install(){
    local giturl=https://github.com/junegunn/fzf.git
    local pkgdir="${PEARL_PKGVARDIR}/${PEARL_PKGNAME}"

    # Install/update the dependency here:
    # pearl emerge PEARL_REPO_NAME/mydep

    info "Installing or updating the ${PEARL_PKGNAME} git repository..."
    install_or_update_git_repo $giturl $pkgdir master
    $pkgdir/install --all
    if [ -d "$pkgdir/doc" ] && [ `which vim` ]; then
        info "Generating the help tags..."
        vim -u NONE -c "helptags $pkgdir/doc" -c q
    fi

    return 0
}

function post_update(){
    post_install
}

function pre_remove(){
    ${PEARL_PKGVARDIR}/${PEARL_PKGNAME}/uninstall
    rm -rf "${PEARL_PKGVARDIR}"

    # Uncomment below to strictly remove the dependency
    # pearl remove PEARL_PKGREPONAME/mydep

    return 0
}

# vim: ft=sh
